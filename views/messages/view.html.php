<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class ContactViewMessages extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	public function display($tpl = null)
	{
    $this->addTemplatePath(__DIR__.'/tmpl/');
    $this->addTemplatePath(JPATH_THEMES.'/'.JFactory::getApplication()->getTemplate().'/html/com_contact/messages/'); 
    
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');

		ContactHelper::addSubmenu('');

		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	protected function addToolbar()
	{
		$canDo	= JHelperContent::getActions('com_contact', 'category', $this->state->get('filter.category_id'));
		$user	= JFactory::getUser();

		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_CONTACT_MANAGER_CONTACTS'), 'mail contact');

		if ($canDo->get('core.edit.state'))
		{
			JToolbarHelper::archiveList('contacts.archive');
		}
    
		if ($this->state->get('filter.published') == -2 && $canDo->get('core.delete'))
		{
			JToolbarHelper::deleteList('', 'contacts.delete', 'JTOOLBAR_EMPTY_TRASH');
		}
		elseif ($canDo->get('core.edit.state'))
		{
			JToolbarHelper::trash('contacts.trash');
		}

		if ($user->authorise('core.admin', 'com_contact'))
		{
			JToolbarHelper::preferences('com_contact');
		}

		JToolbarHelper::help('JHELP_COMPONENTS_CONTACTS_CONTACTS');

		JHtmlSidebar::setAction('index.php?option=com_contact&view=messages');

    JHtml::addIncludePath(JPATH_PLUGINS.'/system/n3tcontact/helpers/html');
    
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_PUBLISHED'),
			'filter_published',
			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions', array('published' => 0, 'unpublished' => 0)), 'value', 'text', $this->state->get('filter.published'), true)
		);

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_CATEGORY'),
			'filter_category_id',
			JHtml::_('select.options', JHtml::_('category.options', 'com_contact'), 'value', 'text', $this->state->get('filter.category_id'))
		);

		/*JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_CONTACT'),
			'filter_contact_id',
			JHtml::_('select.options', JHtml::_('n3tcontact.jgridContactOptions', $this->state->get('filter.category_id')), 'value', 'text', $this->state->get('filter.contact_id'))
		);*/

	}

	protected function getSortFields()
	{
		return array(
			'a.created' => JText::_('PLG_SYSTEM_N3TCONTACT_JGRID_HEADING_CREATED'),
			'a.published' => JText::_('JSTATUS'),
			'a.name' => JText::_('PLG_SYSTEM_N3TCONTACT_JGRID_HEADING_NAME'),
      'a.email' => JText::_('PLG_SYSTEM_N3TCONTACT_JGRID_HEADING_EMAIL'),
      'a.subject' => JText::_('PLG_SYSTEM_N3TCONTACT_JGRID_HEADING_SUBJECT'),
      'contact_name' => JText::_('PLG_SYSTEM_N3TCONTACT_JGRID_HEADING_CONTACT_NAME'),
      'a.email_copy' => JText::_('PLG_SYSTEM_N3TCONTACT_JGRID_HEADING_EMAIL_COPY'),
			'category_title' => JText::_('JCATEGORY'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}
}
