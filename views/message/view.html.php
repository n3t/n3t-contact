<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class ContactViewMessage extends JViewLegacy
{

	public function display($tpl = null)
	{
    $this->addTemplatePath(__DIR__.'/tmpl/');
    $this->addTemplatePath(JPATH_THEMES.'/'.JFactory::getApplication()->getTemplate().'/html/com_contact/message/');
    
 		$this->form  = $this->get('Form');
		$this->item  = $this->get('Item');
		$this->state = $this->get('State');

		$this->addToolbar();
		parent::display($tpl);
	}

	protected function addToolbar()
	{
		JToolbarHelper::title(JText::_('PLG_SYSTEM_N3TCONTACT_SIDEBAR_MESSAGE'), 'mail contact');
		JToolbarHelper::cancel('message.cancel', 'JTOOLBAR_CLOSE');
    JHtml::addIncludePath(JPATH_PLUGINS.'/system/n3tcontact/helpers/html');
	}
}
