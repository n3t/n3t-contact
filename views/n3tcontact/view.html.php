<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\CMS\Version;
use Joomla\CMS\MVC\View\GenericDataException;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Factory;

class ContactViewN3tContact extends JViewLegacy
{
	protected $form;

	protected $item;

	protected $return_page;

	public function display($tpl = null)
	{
    $app = Factory::getApplication();
    if (Version::MAJOR_VERSION === 3) {
      $user = Factory::getUser();
    } else {
      $user = $app->getIdentity();
    }
		$item       = $this->get('Item');
		$this->form = $this->get('Form');

		$params = JComponentHelper::getParams('com_contact');

		if ($item)
			$params->merge($item->params);

		if (count($errors = $this->get('Errors')))
		{
      if (Version::MAJOR_VERSION === 3) {
        JError::raiseWarning(500, implode("\n", $errors));
      } else {
        throw new GenericDataException(implode("\n", $errors), 500);
      }

			return false;
		}

		$groups	= $user->getAuthorisedViewLevels();

		$return = '';

		if ((!in_array($item->access, $groups)) || (!in_array($item->category_access, $groups)))
		{
      if (Version::MAJOR_VERSION === 3) {
        JError::raiseWarning(403, Text::_('JERROR_ALERTNOAUTHOR'));
      } else {
        $app->enqueueMessage(Text::_('JERROR_ALERTNOAUTHOR'), 'error');
        $app->setHeader('status', 403, true);
      }
			return false ;
		}

		$this->contact  = &$item;
		$this->params   = &$params;
		$this->return   = &$return;

		$model = $this->getModel();
		$model->hit();

    $result = $this->loadTemplate($tpl);
		if ($result instanceof Exception)
			return '';
		return $result;
	}

}
