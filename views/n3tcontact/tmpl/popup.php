<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('jquery.framework');
JHtml::_('jquery.ui');
JHtml::_('bootstrap.framework');
JHtml::_('script','plg_system_n3tcontact/n3tcontact.min.js', ['relative' => true]);
JHtml::_('stylesheet','plg_system_n3tcontact/n3tcontact.min.css', ['relative' => true]);

JText::script('PLG_SYSTEM_N3TCONTACT_JS_UNKNOWN_ERROR',true);
JText::script('PLG_SYSTEM_N3TCONTACT_JS_SUCCESS',true);
JText::script('PLG_SYSTEM_N3TCONTACT_JS_AJAX_ERROR',true);
$id = uniqid();
?>

<a href="#<?php echo $id; ?>" class="btn btn-primary btn-modal" data-toggle="modal"><?php echo $this->contact->name; ?></a>

<div class="modal hide fade modal-contact-form" id="<?php echo $id; ?>">
  <form id="contact-form" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate form-horizontal n3tContactAjax" enctype="multipart/form-data">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3><?php echo $this->contact->name; ?></h3>
    </div>

    <div class="modal-body">
      <?php if (isset($this->error)) { ?>
      	<div class="contact-error">
      		<?php echo $this->error; ?>
      	</div>
      <?php } ?>

      <div class="message-container"></div>
			<?php
        foreach ($this->form->getFieldsets() as $fieldset) {
          if ($fieldset->name != 'captcha') {
            $this->fieldset = $fieldset->name;
            echo $this->loadTemplate('fieldset');
          }
			  }
        $this->fieldset = 'captcha';
        echo $this->loadTemplate('fieldset');
      ?>

			<input type="hidden" name="option" value="com_contact" />
			<input type="hidden" name="task" value="contact.submit" />
			<input type="hidden" name="return" value="<?php echo $this->return_page; ?>" />
			<input type="hidden" name="id" value="<?php echo $this->contact->slug; ?>" />
			<?php echo JHtml::_('form.token'); ?>
    </div>

    <div class="modal-footer">
      <a href="#" class="btn" data-dismiss="modal"><?php echo JText::_('PLG_SYSTEM_N3TCONTACT_POPUP_CLOSE'); ?></a>
      <button class="btn btn-primary validate" type="submit"><?php echo JText::_('COM_CONTACT_CONTACT_SEND'); ?></button>
    </div>
  </form>
</div>
