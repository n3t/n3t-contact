<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Version;

if (Version::MAJOR_VERSION === 3) {
	HTMLHelper::_('behavior.keepalive');
	HTMLHelper::_('behavior.formvalidation');
}
HTMLHelper::_('jquery.framework');
HTMLHelper::_('script','plg_system_n3tcontact/n3tcontact.min.js',false,true);
HTMLHelper::_('stylesheet','plg_system_n3tcontact/n3tcontact.min.css',false,true);

Text::script('PLG_SYSTEM_N3TCONTACT_JS_UNKNOWN_ERROR',true);
Text::script('PLG_SYSTEM_N3TCONTACT_JS_SUCCESS',true);
Text::script('PLG_SYSTEM_N3TCONTACT_JS_AJAX_ERROR',true);

if (isset($this->error)) { ?>
	<div class="contact-error">
		<?php echo $this->error; ?>
	</div>
<?php } ?>

<div class="contact-form">
	<form id="contact-form" action="<?php echo Route::_('index.php'); ?>" method="post" class="form-validate form-horizontal n3tContactAjax" enctype="multipart/form-data">
		<fieldset>
			<div class="message-container"></div>
			<?php
        foreach ($this->form->getFieldsets() as $fieldset) {
          if ($fieldset->name != 'captcha') {
            $this->fieldset = $fieldset->name;
            echo $this->loadTemplate('fieldset');
          }
			  }
        $this->fieldset = 'captcha';
        echo $this->loadTemplate('fieldset');
      ?>
			<div class="form-actions">
				<button class="btn btn-primary validate" type="submit"><?php echo Text::_('COM_CONTACT_CONTACT_SEND'); ?></button>
				<input type="hidden" name="option" value="com_contact" />
				<input type="hidden" name="task" value="contact.submit" />
				<input type="hidden" name="return" value="<?php echo $this->return_page; ?>" />
				<input type="hidden" name="id" value="<?php echo $this->contact->slug; ?>" />
				<?php echo HTMLHelper::_('form.token'); ?>
			</div>
		</fieldset>
	</form>
</div>
