<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2017 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

$fields = $this->form->getFieldset($this->fieldset);
foreach ($fields as $field)
  echo $field->renderField();
