<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

JFormHelper::loadFieldClass('checkbox');

class JFormFieldN3tTOS extends JFormFieldCheckbox
{
	protected $type = 'n3ttos';

  protected $tospage = null;

	protected function getLayoutData()
	{
    $data = parent::getLayoutData();

    if ($this->tospage) {
      $label = '<a href="' . JRoute::_('index.php?Itemid=' . $this->tospage) . '"';
      if ($this->linktarget)
        $label .= ' target="' . $this->linktarget . '"';
      $label .= '>' . $data['label'] . '</a>';
      $data['label'] = $label;
    }

    return $data;
	}

	protected function getLabel()
	{
    return '&nbsp;';
	}

	protected function getInput()
	{
    return parent::getInput() . '&nbsp;' . parent::getLabel();
	}

  public function __get($name)
	{
		switch ($name) {
			case 'tospage':
      case 'linktarget':
				return $this->$name;
		}

		return parent::__get($name);
	}

	public function __set($name, $value)
	{
		switch ($name) {
			case 'tospage':
				$this->$name = (int)$value;
				break;

			case 'linktarget':
				$this->$name = $value;
				break;

			default:
				parent::__set($name, $value);
		}
	}

	public function setup(SimpleXMLElement $element, $value, $group = null)
	{
		$return = parent::setup($element, $value, $group);

		if ($return) {
			$this->tospage = (int)$this->element['tospage'];
      $this->linktarget = (string)$this->element['linktarget'];
    }

		return $return;
	}
}
