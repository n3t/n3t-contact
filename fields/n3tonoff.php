<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

JFormHelper::loadFieldClass('checkbox');

class JFormFieldN3tOnOff extends JFormFieldCheckbox
{
	protected $type = 'n3tonoff';

  protected $texton = null;
  protected $textoff = null;

	protected function getInput()
	{
    $return = parent::getInput();
    return '<span class="n3t-onoff">' . $return . '<label for="' . $this->id . '"><span>' . $this->texton . '</span><span>' . $this->textoff . '</span></label></span>';
	}
   
  public function __get($name)
	{
		switch ($name) {
			case 'texton':
      case 'textoff':
				return $this->$name;
		}

		return parent::__get($name);
	}

	public function __set($name, $value)
	{
		switch ($name) {
			case 'texton':
      case 'textoff':
				$this->$name = JText::_($value);
				break;

			default:
				parent::__set($name, $value);
		}
	}

	public function setup(SimpleXMLElement $element, $value, $group = null)
	{
		$return = parent::setup($element, $value, $group);

		if ($return) {
			$this->texton = JText::_((string)$this->element['texton'] ?: 'JYES');
      $this->textoff = JText::_((string)$this->element['textoff'] ?: 'JNO');
		}

		return $return;
	}
}
