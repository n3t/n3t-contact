<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

JFormHelper::loadFieldClass('email');

class JFormFieldN3tContactEMail extends JFormFieldEMail
{

	protected $type = 'n3tcontactemail';

	protected function getInput()
	{
		return str_replace('@', '&#64;', parent::getInput());
	}
}
