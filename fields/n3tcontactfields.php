<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\CMS\Form\FormField;

class JFormFieldN3tContactFields extends FormField
{
	protected $type = 'n3tcontactfields';

	protected function getInput()
	{
		return n3tContactField::getTable($this->value);
	}
   
	protected function getLabel() {
    return '';
  }

  public function renderField($options = array()) {
    return $this->getInput();
  }

}
