<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Form\FormField;
use Joomla\CMS\Language\Text;

class JFormFieldN3tContactPluginCode extends FormField
{
	protected $type = 'n3tcontactplugincode';

	protected function getInput()
	{
	  $id = Factory::getApplication()->input->getInt('id');
    $value = $id ? '{n3tcontact ' . $id . '}' : Text::_('PLG_SYSTEM_N3TCONTACT_PLUGIN_CODE_SAVE_FIRST');
		return '<input type="text" value="' . htmlspecialchars($value) . '" class="form-control" size="30" readonly />';
	}
   
}
