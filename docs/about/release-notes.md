Release notes
=============

#### Next release

  * storing send emails option
  * new popup layout
  * option to define custom layouts
  * load contact form by alias
  * JS event on success called before form reset now.

3.0.x
-----

#### 3.0.12

  * new option to specify layout file of custom thank you message
  * new xreference option for all fields
  * TOS field link target option
  * corrected checkbox, On/Off and TOS fields rendering in text emails.

#### 3.0.11

  * fatal error with On/Off field solved

#### 3.0.10

  * fatal error with TOS field solved

#### 3.0.9

  * incorrect scrolling to messages solved

#### 3.0.8

  * new field type On/Off switch
  * new field type TOS agreement

#### 3.0.7

  * multiple forms on one page (any page) loads correctly now

#### 3.0.6

  * moving custom fields row do not loose Yes/No values anymore
  * Guest could not access messages view anymore
  * Hidden e-mail and name options
  * [fieldsall] macro for email content
  * option to select CAPTCHA type (or disable) per contact

#### 3.0.5

  * custom thank you message
  * custom thank you HTML page
  * new SVG loader icon

#### 3.0.4

  * improved display of validation messages
  * file fileds can be displayed in email
  * new number field

#### 3.0.3

  * multiple forms on contact page now loads correctly

#### 3.0.2

  * multiple forms on one page now loads correctly

#### 3.0.1

  * validation messages for ajax forms displays now in the form area

#### 3.0.0

 * Initial release       
