<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

// HTML Text of custom thank you message
$text = $displayData['text'];
// Contact
$contact = $displayData['contact'];
// submitted data
$data = $displayData['data'];
?>
<div class="n3tContactThankYou">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3><?php echo $contact->name; ?></h3>
  </div>

  <div class="modal-body">
    <?php echo $text; ?>
  </div>

  <div class="modal-footer">
    <a href="#" class="btn btn-primary" data-dismiss="modal"><?php echo JText::_('PLG_SYSTEM_N3TCONTACT_POPUP_CLOSE'); ?></a>
  </div>
</div>