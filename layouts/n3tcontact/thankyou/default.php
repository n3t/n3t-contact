<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

// HTML Text of custom thank you message
$text = $displayData['text'];
// Contact
$contact = $displayData['contact'];
// submitted data
$data = $displayData['data'];
?>
<div class="n3tContactThankYou">
<?php echo $text; ?>
</div>