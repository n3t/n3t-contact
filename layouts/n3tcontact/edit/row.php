<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

$form = $displayData['form'];
$data = $displayData['data'];
$index = $displayData['index'];
?>
  <tr class="n3tCustomField">
<?php
$fields = $form->getFieldset('main'); 
foreach ($fields as $field) {
  n3tContactField::createUniqueId($field);
?> 
    <td><?php echo $field->input; ?></td>
<?php } ?>
  </tr>
  <tr class="n3tCustomFieldOptions" style="display: none;">
    <td style="border-top: none;">&nbsp;</td>
    <td colspan="<?php echo count($fields) - 1; ?>" style="border-top: none;">
    <?php echo n3tContactField::getOptionsRow(isset($data['type']) ? $data['type'] : 'text', $data, $index); ?>    
    </td>
  </tr>
  
