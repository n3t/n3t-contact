<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

$rows = $displayData['rows'];

echo n3tContactField::getToolbar();
?>
<table id="n3tContactFields" class="table">
<?php 
  echo n3tContactField::getHeader(); 
  if (is_array($rows)) {
    foreach($rows as $index=>$row) {
?>
  <tbody>
    <?php echo n3tContactField::getRow($row,$index); ?> 
  </tbody>
<?php      
    }
  }
?>  
</table>

