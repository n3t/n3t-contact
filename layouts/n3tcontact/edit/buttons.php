<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;
?>
<div class="btn-group">
  <button onclick="n3tContact.removeField(this); return false;" class="btn btn-small btn-danger hasTooltip" title="<?php echo JText::_('PLG_SYSTEM_N3TCONTACT_BUTTON_DELETE'); ?>">
    <span class="icon-delete"></span>
  </button>
  <button onclick="n3tContact.moveField(this,-1); return false;" class="btn btn-small hasTooltip" title="<?php echo JText::_('PLG_SYSTEM_N3TCONTACT_BUTTON_UP'); ?>">
    <span class="icon-arrow-up"></span>
  </button>
  <button onclick="n3tContact.moveField(this,1); return false;" class="btn btn-small hasTooltip" title="<?php echo JText::_('PLG_SYSTEM_N3TCONTACT_BUTTON_DOWN'); ?>">
    <span class="icon-arrow-down"></span>
  </button>
  <button onclick="n3tContact.toggleOptions(this); return false;" class="btn btn-small btn-info hasTooltip" title="<?php echo JText::_('PLG_SYSTEM_N3TCONTACT_BUTTON_OPTIONS'); ?>">
    <span class="icon-options"></span>
  </button>
</div>
