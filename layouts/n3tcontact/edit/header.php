<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

$form = $displayData['form'];
?>
<thead>
  <tr>
<?php
$fields = $form->getFieldset('main'); 
foreach ($fields as $field) { ?> 
    <th><?php echo $field->label; ?></th>
<?php } ?>
  </tr>
</thead>