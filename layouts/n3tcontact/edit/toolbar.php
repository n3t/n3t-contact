<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

use Joomla\CMS\Language\Text;

defined('_JEXEC') or die;

?>
<div id="n3tContactToolbar" class="btn-toolbar">
  <div class="btn-wrapper">
    <button onclick="n3tContact.addField(); return false;" class="btn btn-small btn-success">
      <span class="icon-new"></span> <?php echo Text::_('PLG_SYSTEM_N3TCONTACT_NEW_FIELD'); ?>
    </button>
  </div>
</div>
