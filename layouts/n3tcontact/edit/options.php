<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

$form = $displayData['form'];

foreach ($form->getFieldsets() as $fieldset) { 
?>
  <?php $fields = $form->getFieldset($fieldset->name); ?>
  <?php 
    foreach ($fields as $field) {
      n3tContactField::createUniqueId($field); 
  ?> 
  <div class="control-group">
    <div class="control-label"><?php echo $field->label; ?></div>
    <div class="controls"><?php echo $field->input; ?></div>
  </div>
  <?php } ?>
<?php } ?>