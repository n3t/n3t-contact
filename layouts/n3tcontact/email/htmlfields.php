<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

$data = $displayData['data'];
$contact = $displayData['contact'];
$custom_fields = $displayData['fields'];
$includeBasicFields = $displayData['includeBasicFields'];

if ($custom_fields || $includeBasicFields) {
?>
<table cellspacing="2" border="0">  
<?php
  if ($includeBasicFields) {
    if (@$data['contact_name']) {
?>
  <tr>
    <th align="left" valign="top"><?php echo $contact->params->get('n3t_name_label',JText::_('COM_CONTACT_CONTACT_EMAIL_NAME_LABEL')); ?>:</th>
    <td align="left" valign="top"><?php echo $data['contact_name']; ?></td>
  </tr>
<?php
    }

    if (@$data['contact_email'] && !$contact->params->get('n3t_email_hidden',0)) {
?>
  <tr>
    <th align="left" valign="top"><?php echo $contact->params->get('n3t_email_label',JText::_('COM_CONTACT_EMAIL_LABEL')); ?>:</th>
    <td align="left" valign="top"><?php echo $data['contact_email']; ?></td>
  </tr>
<?php
    }

    if (@$data['contact_subject'] && !$contact->params->get('n3t_subject_hidden',0)) {
?>
  <tr>
    <th align="left" valign="top"><?php echo $contact->params->get('n3t_subject_label',JText::_('COM_CONTACT_CONTACT_MESSAGE_SUBJECT_LABEL')); ?>:</th>
    <td align="left" valign="top"><?php echo $data['contact_subject']; ?></td>
  </tr>
<?php
    }

    if (@$data['contact_message'] && !$contact->params->get('n3t_message_hidden',0)) {
?>
  <tr>
    <th align="left" valign="top"><?php echo $contact->params->get('n3t_message_label',JText::_('COM_CONTACT_CONTACT_ENTER_MESSAGE_LABEL')); ?>:</th>
    <td align="left" valign="top"><?php echo $data['contact_message']; ?></td>
  </tr>
<?php
    }
  }
?>
<?php
  foreach ($custom_fields as $index => $field) {
    if (n3tContactEmail::isHidden($field['type'])) continue;
?>
  <tr>
    <th align="left" valign="top"><?php echo $field['label']; ?>:</th>
    <td align="left" valign="top"><?php echo n3tContactEmail::fieldValue($field, @$data['n3t_custom_fields']['f'.$index], true); ?></td>
  </tr>
<?php
  }
?>
</table>  
<?php  
}   