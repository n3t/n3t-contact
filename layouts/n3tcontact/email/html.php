<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

$body = $displayData['body'];
$head = $displayData['head'];
$copy = $displayData['copy'];
$copy_text = $displayData['copy_text'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if ($copy_text) { ?>
<p><?php echo $copy_text; ?></p>
<?php } ?>
<?php echo $head; ?>
</head>
<body>
<?php echo $body; ?>
</body>
</html>