<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

$field = $displayData['field'];
$value = $displayData['value'];

if (!n3tContactEmail::isHidden($field['type'])) {
  if (is_array($value)) {
    if(!empty($value))
      echo '<ul><li>'.implode('</li><li>',$value).'</li></ul>';
  } else if ($field['type'] == 'textarea')
    echo '<p>'.implode('<br />', preg_split('/\r?\n/', $value)).'</p>';
  else if ($field['type'] == 'checkbox')
    echo $value ? JText::_('JYES') : JText::_('JNO');
  else if ($field['type'] == 'url' && $value)    
    echo '<a href="'.$value.'">'.$value.'</a>';
  else if ($field['type'] == 'color' && $value)  
    echo '<span style="color: '.$value.';">'.$value.'</span>';
  else if ($field['type'] == 'n3tonoff')
    echo $value ? ($field['texton'] ? JText::_($field['texton']) : JText::_('JYES')) : ($field['textoff'] ? JText::_($field['textoff']) : JText::_('JNO'));
  else if ($field['type'] == 'n3ttos')
    echo $value ? JText::_('JYES') : JText::_('JNO');
  else
    echo $value;    
}  