<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\Helpers\Sidebar;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Plugin\CMSPlugin;
use Joomla\CMS\Version;
use Joomla\Registry\Registry;
use Joomla\String\StringHelper;
use Joomla\CMS\Menu\AdministratorMenuItem;
use Joomla\CMS\Form\FormHelper;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Table\Table;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use Joomla\CMS\MVC\Controller\BaseController;
use Joomla\Component\Contact\Site\Model\ContactModel;

class plgSystemN3tContact extends CMSPlugin
{
    protected $autoloadLanguage = true;

    protected $contact_id = null;

    function __construct(& $subject, $config)
    {
        parent::__construct($subject, $config);

        $this->loadHelper('layout');
        if (!Factory::getApplication()->isClient('site')) {
            $this->loadHelper('field');
        }
    }

    private function loadHelper($file) {
        JLoader::register('n3tContact'.ucfirst($file), __DIR__ . '/helpers/'.$file.'.php');
    }

    private function loadContact($contact_id = null): ?object {
        $app = Factory::getApplication();
        if ($contact_id === null) {
            if ($this->contact_id !== null) {
                $contact_id = $this->contact_id;
                $this->contact_id = null;
            } elseif ($app->input->getCmd('option') == 'com_contact'
                && $app->input->getCmd('view') == 'contact') {
                $contact_id = $app->input->getUint('id', 0);
            } else if ($app->getUserState('com_contact.contact.id')) {
                $contact_id = $app->getUserState('com_contact.contact.id');
            }
        }

        if ($contact_id) {
            if (Version::MAJOR_VERSION == 3) {
                require_once(JPATH_ROOT . '/components/com_contact/models/contact.php');
                $model = new ContactModelContact();

                $errorHandling = JError::getErrorHandling(E_ERROR);
                JError::setErrorHandling(E_ERROR, 'ignore');
                try {
                    $item = $model->getItem($contact_id);

                    if (!isset($item->id)) {
                        return null;
                    }

                    return $item;
                } finally {
                    JError::setErrorHandling(E_ERROR, $errorHandling['mode'], isset($errorHandling['options']) ? $errorHandling['options'] : []);
                }
            } else {
                require_once(JPATH_ROOT . '/components/com_contact/src/Model/ContactModel.php');
                $model = new ContactModel();
                try {
                    return $model->getItem($contact_id);
                } catch (\Exception $e) {
                    return null;
                }
            }
        }

        return null;
    }

    private function xmlAttribute($value) {
        $value = str_replace("\n", '&#10;', $value);
        $value = str_replace("\r", '&#13;', $value);
        $value = str_replace("\t", '&#9;', $value);
        return $value;
    }

    private function loadFrontendForm($form)
    {
        $app = Factory::getApplication();
        $contact = $this->loadContact();

        if ($contact) {
            FormHelper::addFieldPath(__DIR__.'/fields');

            if ($contact->params->get('n3t_spacer_hidden',0))
                $form->removeField('spacer');

            if ($contact->params->get('n3t_name_label',''))
                $form->setFieldAttribute('contact_name', 'label', $contact->params->get('n3t_name_label',''));
            if ($contact->params->get('n3t_name_desc',''))
                $form->setFieldAttribute('contact_name', 'description', $contact->params->get('n3t_name_desc',''));
            if ($contact->params->get('n3t_name_hint',''))
                $form->setFieldAttribute('contact_name', 'hint', $contact->params->get('n3t_name_hint',''));
            elseif ($contact->params->get('n3t_label_hint',0))
                $form->setFieldAttribute('contact_name', 'hint', $form->getFieldAttribute('contact_name', 'label'));
            $form->setFieldAttribute('contact_name', 'class', implode(' ', array($form->getFieldAttribute('contact_name', 'class'), $contact->params->get('n3t_name_class',''))));
            if ($contact->params->get('n3t_name_hidden',0)) {
                $form->setFieldAttribute('contact_name', 'type', 'hidden');
                $form->setFieldAttribute('contact_name', 'required', 'false');
            }

            // to avoid email cloaking in content plugin
            $form->setFieldAttribute('contact_email', 'type', 'n3tcontactemail');
            if ($contact->params->get('n3t_email_label',''))
                $form->setFieldAttribute('contact_email', 'label', $contact->params->get('n3t_email_label',''));
            if ($contact->params->get('n3t_email_desc',''))
                $form->setFieldAttribute('contact_email', 'description', $contact->params->get('n3t_email_desc',''));
            if ($contact->params->get('n3t_email_hint',''))
                $form->setFieldAttribute('contact_email', 'hint', $contact->params->get('n3t_email_hint',''));
            elseif ($contact->params->get('n3t_label_hint',0))
                $form->setFieldAttribute('contact_email', 'hint', $form->getFieldAttribute('contact_email', 'label'));
            $form->setFieldAttribute('contact_email', 'class', implode(' ', array($form->getFieldAttribute('contact_email', 'class'), $contact->params->get('n3t_email_class',''))));
            if ($contact->params->get('n3t_email_hidden',0)) {
                $form->setFieldAttribute('contact_email', 'type', 'hidden');
                $form->setFieldAttribute('contact_email', 'required', 'false');
            }

            if ($contact->params->get('n3t_subject_label',''))
                $form->setFieldAttribute('contact_subject', 'label', $contact->params->get('n3t_subject_label',''));
            if ($contact->params->get('n3t_subject_desc',''))
                $form->setFieldAttribute('contact_subject', 'description', $contact->params->get('n3t_subject_desc',''));
            if ($contact->params->get('n3t_subject_hint',''))
                $form->setFieldAttribute('contact_subject', 'hint', $contact->params->get('n3t_subject_hint',''));
            elseif ($contact->params->get('n3t_label_hint',0))
                $form->setFieldAttribute('contact_subject', 'hint', $form->getFieldAttribute('contact_subject', 'label'));
            $form->setFieldAttribute('contact_subject', 'class', implode(' ', array($form->getFieldAttribute('contact_subject', 'class'), $contact->params->get('n3t_subject_class',''))));
            if (!$form->getValue('contact_subject')) {
                if ($contact->params->get('n3t_subject_default_url_param','')
                    && $app->input->get($contact->params->get('n3t_subject_default_url_param',''), '', 'string'))
                    $form->setValue('contact_subject', null, $app->input->get($contact->params->get('n3t_subject_default_url_param',''), '', 'string'));
                elseif ($contact->params->get('n3t_subject_default',''))
                    $form->setValue('contact_subject', null, $contact->params->get('n3t_subject_default',''));
            }
            if ($contact->params->get('n3t_subject_hidden',0)) {
                $form->setFieldAttribute('contact_subject', 'type', 'hidden');
                $form->setFieldAttribute('contact_subject', 'required', 'false');
            }

            if ($contact->params->get('n3t_copy_label',''))
                $form->setFieldAttribute('contact_email_copy', 'label', $contact->params->get('n3t_copy_label',''));
            if ($contact->params->get('n3t_copy_desc',''))
                $form->setFieldAttribute('contact_email_copy', 'description', $contact->params->get('n3t_copy_desc',''));
            $form->setFieldAttribute('contact_email_copy', 'class', implode(' ', array($form->getFieldAttribute('contact_email_copy', 'class'), $contact->params->get('n3t_copy_class',''))));
            $form->setValue('contact_email_copy', null, $contact->params->get('n3t_copy_default',0));
            if ($contact->params->get('n3t_copy_hidden',0)) {
                $form->setFieldAttribute('contact_email_copy', 'type', 'hidden');
            }

            if ($contact->params->get('n3t_message_label',''))
                $form->setFieldAttribute('contact_message', 'label', $contact->params->get('n3t_message_label',''));
            if ($contact->params->get('n3t_message_desc',''))
                $form->setFieldAttribute('contact_message', 'description', $contact->params->get('n3t_message_desc',''));
            if ($contact->params->get('n3t_message_hint',''))
                $form->setFieldAttribute('contact_message', 'hint', $contact->params->get('n3t_message_hint',''));
            elseif ($contact->params->get('n3t_label_hint',0))
                $form->setFieldAttribute('contact_message', 'hint', $form->getFieldAttribute('contact_message', 'label'));
            $form->setFieldAttribute('contact_message', 'class', implode(' ', array($form->getFieldAttribute('contact_message', 'class'), $contact->params->get('n3t_message_class',''))));
            if (!$form->getValue('contact_message')) {
                if ($contact->params->get('n3t_message_default_url_param','')
                    && $app->input->get($contact->params->get('n3t_message_default_url_param',''), '', 'string'))
                    $form->setValue('contact_message', null, $app->input->get($contact->params->get('n3t_message_default_url_param',''), '', 'string'));
                elseif ($contact->params->get('n3t_message_default',''))
                    $form->setValue('contact_message', null, $contact->params->get('n3t_message_default',''));
            }
            if ($contact->params->get('n3t_message_hidden',0)) {
                $form->setFieldAttribute('contact_message', 'type', 'hidden');
                $form->setFieldAttribute('contact_message', 'required', 'false');
            }

            if ($contact->params->get('captcha','') !== '')
                $app->getParams()->set('captcha', $contact->params->get('captcha',''));


            $custom_fields = $contact->params->get('n3t_custom_fields',null);
            if ($custom_fields) {
                $xml = '<?xml version="1.0" encoding="utf-8"?'.'>';
                $xml.= '<form>';
                $xml.= '<fields name="n3t_custom_fields">';
                $xml.= '<fieldset name="n3t_custom_fields">';
                foreach ($custom_fields as $index => $field) {
                    $options = array('type', 'label', 'description');
                    $true_options = array('required');
                    $false_options = array();

                    if ($contact->params->get('n3t_label_hint',0) && empty($field['hint']))
                        $field['hint'] = $field['label'];

                    switch ($field['type']) {
                        case 'calendar':
                            $options = array_merge($options, array('default', 'class', 'hint', 'filter', 'format'));
                            $true_options = array_merge($true_options, array('readonly', 'disabled'));
                            $false_options = array_merge($false_options, array('autocomplete'));
                            break;
                        case 'checkbox':
                            $options = array_merge($options, array('class'));
                            $true_options = array_merge($true_options, array('checked', 'disabled'));
                            break;
                        case 'color':
                            $options = array_merge($options, array('default', 'control', 'position', 'class', 'colors', 'split'));
                            $false_options = array_merge($false_options, array('autocomplete'));
                            break;
                        case 'email':
                            // to avoid email cloaking in content plugin
                            $field['type'] = 'n3tcontactemail';
                            $options = array_merge($options, array('default', 'size', 'maxlength', 'class', 'hint'));
                            $true_options = array_merge($true_options, array('readonly', 'disabled'));
                            $false_options = array_merge($false_options, array('autocomplete'));
                            break;
                        case 'file':
                            $options = array_merge($options, array('accept', 'size', 'class'));
                            $true_options = array_merge($true_options, array('multiple', 'disabled'));
                            break;
                        case 'list':
                            $options = array_merge($options, array('default', 'class', 'size'));
                            $true_options = array_merge($true_options, array('readonly', 'multiple'));
                            break;
                        case 'note':
                            $options = array_merge($options, array('heading', 'class'));
                            $true_options = array_merge($true_options, array('close'));
                            break;
                        case 'number':
                            $options = array_merge($options, array('default', 'size', 'maxlength', 'class', 'hint', 'filter', 'min', 'max', 'step'));
                            $true_options = array_merge($true_options, array('readonly', 'disabled'));
                            $false_options = array_merge($false_options, array('autocomplete'));
                            break;
                        case 'password':
                            $options = array_merge($options, array('size', 'maxlength', 'class', 'hint', 'treshold'));
                            $true_options = array_merge($true_options, array('readonly', 'disabled', 'strengthmeter'));
                            $false_options = array_merge($false_options, array('autocomplete'));
                            break;
                        case 'radio':
                            $options = array_merge($options, array('default', 'class'));
                            $true_options = array_merge($true_options, array('readonly', 'disabled'));
                            break;
                        case 'spacer':
                            $options = array_merge($options, array('class'));
                            $true_options = array_merge($true_options, array('hr'));
                            break;
                        case 'tel':
                            $options = array_merge($options, array('default', 'size', 'maxlength', 'class', 'hint'));
                            $true_options = array_merge($true_options, array('readonly', 'disabled'));
                            $false_options = array_merge($false_options, array('autocomplete'));
                            break;
                        case 'text':
                            $options = array_merge($options, array('default', 'size', 'maxlength', 'class', 'hint', 'filter'));
                            $true_options = array_merge($true_options, array('readonly', 'disabled'));
                            $false_options = array_merge($false_options, array('autocomplete'));
                            break;
                        case 'url':
                            $options = array_merge($options, array('default', 'size', 'maxlength', 'class', 'hint'));
                            $true_options = array_merge($true_options, array('readonly', 'disabled'));
                            $false_options = array_merge($false_options, array('autocomplete'));
                            break;
                        case 'textarea':
                            $options = array_merge($options, array('default', 'class', 'cols', 'rows', 'hint', 'filter'));
                            $true_options = array_merge($true_options, array('readonly', 'disabled'));
                            $false_options = array_merge($false_options, array('autocomplete'));
                            break;
                        case 'hidden':
                            $options = array_merge($options, array('default'));
                            break;
                        case 'n3tonoff':
                            $options = array_merge($options, array('class', 'texton', 'textoff'));
                            $true_options = array_merge($true_options, array('checked', 'disabled'));
                            break;
                        case 'n3ttos':
                            $options = array_merge($options, array('class', 'tospage', 'linktarget'));
                            $true_options = array_merge($true_options, array('checked', 'disabled'));
                            break;
                    }

                    $attributes = array();
                    $attributes['name'] = 'f'.$index;
                    foreach ($options as $option)
                        if (isset($field[$option]) && $field[$option])
                            $attributes[$option] = $this->xmlAttribute($field[$option]);
                    foreach ($true_options as $option)
                        if (isset($field[$option]) && $field[$option])
                            $attributes[$option] = 'true';
                    foreach ($false_options as $option)
                        if (isset($field[$option]) && $field[$option])
                            $attributes[$option] = 'false';

                    $options = array();

                    switch ($field['type']) {
                        case 'calendar':
                            if (isset($field['default_today']) && $field['default_today']
                                && isset($field['default_today_offset']) && $field['default_today_offset'])
                                $attributes['default'] = strftime($field['format'],microtime(true)+$field['default_today_offset']*60*60*24);
                            else if (isset($field['default_today']) && $field['default_today'])
                                $attributes['default'] = 'NOW';
                            break;
                        case 'checkbox':
                            $attributes['value'] = '1';
                            break;
                        case 'color':
                            //$attributes['validate'] = 'color';
                            break;
                        case 'email':
                            $attributes['validate'] = 'email';
                            break;
                        case 'file':
                            break;
                        case 'list':
                            if (isset($field['chosen']) && $field['chosen']) {
                                HTMLHelper::_('formbehavior.chosen');
                                $attributes['class'] = (isset($attributes['class']) && $attributes['class']) ? $attributes['class'].' advancedSelect' : 'advancedSelect';
                            }
                            $required = isset($field['required']) && $field['required'];
                            $multiple = isset($field['multiple']) && $field['multiple'];
                            if ((!$required || $field['empty']) && !$multiple) {
                                $options[] = array('value' => '', 'name' => $field['empty']);
                            }
                            if (isset($field['options']) && $field['options'])
                                $options = array_merge($options, preg_split('/\r?\n/', $field['options']));
                            break;
                        case 'note':
                            $attributes['required'] = 'false';
                            if ($field['heading'] == 'none') {
                                $attributes['label'] = '';
                            }
                            break;
                        case 'number':
                            break;
                        case 'password':
                            break;
                        case 'radio':
                            if (isset($field['options']) && $field['options'])
                                $options = preg_split('/\r?\n/', $field['options']);
                            break;
                        case 'spacer':
                            $attributes['required'] = 'false';
                            break;
                        case 'tel':
                            $attributes['filter'] = 'tel';
                            $attributes['validate'] = 'tel';
                            break;
                        case 'text':
                            break;
                        case 'url':
                            $attributes['filter'] = 'url';
                            $attributes['validate'] = 'url';
                            break;
                        case 'textarea':
                            break;
                        case 'n3tonoff':
                            $attributes['value'] = '1';
                            break;
                        case 'n3ttos':
                            $attributes['value'] = '1';
                            break;
                    }

                    if (isset($field['default_source'])) {
                        switch ($field['default_source']) {
                            case 'current_url':
                                $attributes['default'] = JUri::current();
                                break;
                            case 'current_title':
                                $title = Factory::getDocument()->getTitle();
                                if ($app->get('sitename_pagetitles', 0) == 1)
                                    $title = str_replace(Text::sprintf('JPAGETITLE', $app->get('sitename'), ''), '', $title);
                                elseif ($app->get('sitename_pagetitles', 0) == 2)
                                    $title = str_replace(Text::sprintf('JPAGETITLE', '', $app->get('sitename')), '', $title);
                                $attributes['default'] = $title;
                                break;
                        }
                    }

                    if (isset($field['default_url_param']) && $field['default_url_param']
                        && $app->input->get($field['default_url_param'], '', 'string'))
                        $attributes['default'] = $app->input->get($field['default_url_param'], '', 'string');

                    $xml.= '<field';
                    foreach ($attributes as $name => $value)
                        $xml.= ' '.$name.'="'.htmlspecialchars($value, ENT_COMPAT).'"';
                    $xml.= '>';
                    foreach ($options as $value) {
                        if (is_array($value))
                            $xml.= '<option value="'.htmlspecialchars($value['value'], ENT_COMPAT).'">'.htmlspecialchars($value['name'], ENT_COMPAT).'</option>';
                        else
                            $xml.= '<option value="'.htmlspecialchars($value, ENT_COMPAT).'">'.htmlspecialchars($value, ENT_COMPAT).'</option>';
                    }
                    $xml.= '</field>';
                }
                $xml.= '</fieldset>';
                $xml.= '</fields>';
                $xml.= '</form>';

                $form->load($xml,false);
            }
        }
    }

    private function loadAdminForm($form, $data)
    {
        Form::addFormPath(__DIR__ . '/forms');
        $form->loadFile('n3tcontact', false);

        HTMLHelper::_('jquery.framework');
        HTMLHelper::_('script', 'system/html5fallback.js', ['relative' => true]);
        HTMLHelper::_('script', 'plg_system_n3tcontact/administrator/n3tcontact.min.js', ['relative' => true]);
        HTMLHelper::_('stylesheet', 'plg_system_n3tcontact/administrator/n3tcontact.min.css', ['relative' => true]);
        HTMLHelper::_('bootstrap.tooltip');
        if (Version::MAJOR_VERSION === 3) {
            HTMLHelper::_('behavior.colorpicker');
            HTMLHelper::_('behavior.calendar');
        }

        Text::script('JGLOBAL_SELECT_SOME_OPTIONS', true);
        Text::script('JGLOBAL_SELECT_AN_OPTION', true);
        Text::script('JGLOBAL_SELECT_NO_RESULTS_MATCH', true);
    }

    /**
     * @param Form $form
     * @param $data
     * @return void
     */
    private function loadConfigForm($form, $data)
    {
        //$this->loadAdminForm($form, $data);

        // TODO neumi to removeFieldset :/
        //$form->removeField('captcha', 'params');
    }

    public function onContentPrepareForm($form, $data)
    {
        $app = Factory::getApplication();
        if (!($form instanceof Form)) {
            $this->_subject->setError('JERROR_NOT_A_FORM');
            return false;
        }

        if (!$app->isClient('site')) {
            $this->loadHelper('plugin');
            n3tContactPlugin::addPluginType($this->_type,$this->_name,'editors-xtd');
        }

        $name = $form->getName();
        $app = Factory::getApplication();
        if ($app->isClient('site') && $name == 'com_contact.contact')
            $this->loadFrontendForm($form);
        else {
            if ($name == 'com_contact.contact')
                $this->loadAdminForm($form, $data);
            else if ($name == 'com_config.component' && $app->input->get('component', '', 'cmd') == 'com_contact')
                $this->loadConfigForm($form, $data);
        }
    }

    private function storeEmail($contact, $params, $name, $email, $subject, $text, $html, $values, $attachments, $emailCopy) {
        $message = new stdClass();
        $message->contact_id = $contact->id;
        $message->name = $name;
        $message->email = $email;
        $message->subject = $subject;
        $message->text = $text;
        $message->html = $html;
        $message->values = json_encode($values);
        if (!empty($attachments))
            $message->attachments = $attachments;
        $message->email_copy = $emailCopy;
        $message->created = Factory::getDate()->toSql();
        $message->published = 1;

        $result = Factory::getDbo()->insertObject('#__n3tcontact_messages', $message);
    }

    private function sendEmail($data, $contact, $params) {
        $sent = false;

        $custom_template = $contact->params->get('n3t_use_email_template',false);
        $custom_reply = $contact->params->get('n3t_use_email_copy_template',false);

        if ($custom_template) {
            $this->loadHelper('email');

            $app = Factory::getApplication();

            if ($contact->email_to == '' && $contact->user_id != 0)
            {
                $contact_user      = JUser::getInstance($contact->user_id);
                $contact->email_to = $contact_user->get('email');
            }

            $mailfrom = $params->get('n3t_email_from',$app->get('mailfrom'));
            $fromname = $params->get('n3t_email_from_name',$app->get('fromname'));
            $sitename = $app->get('sitename');

            $name    = $data['contact_name'];
            $email   = JStringPunycode::emailToPunycode($data['contact_email']);

            $body	= $params->get('n3t_email_template','');
            $body_text = $params->get('n3t_email_template_text','');
            $head = $params->get('n3t_email_template_head','');

            $body = n3tContactEmail::processTemplate($data, $contact, $body, true);
            $body_text = n3tContactEmail::processTemplate($data, $contact, $body_text);
            $head = n3tContactEmail::processTemplate($data, $contact, $head);

            $body_html = n3tContactEmail::html($body, $head);
            $subject = $params->get('n3t_email_subject',$sitename . ': ' .$data['contact_subject']);

            $mail = Factory::getMailer();
            $mail->isHTML(true);
            $mail->Encoding = 'base64';

            $mail->addRecipient($contact->email_to);
            $mail->addReplyTo($email, $name);
            $mail->setFrom($mailfrom, $fromname);
            $mail->setSubject($subject);
            $mail->setBody($body_html);
            $mail->AltBody = $body_text;
            $mail->Priority = $params->get('n3t_email_priority', 3);

            n3tContactEmail::processAttachments($contact, $mail);

            $sent = $mail->Send();

            if ($sent instanceof Exception)
                return $sent;

            $emailCopy = $params->get('show_email_copy') == true && !empty($data['contact_email_copy']) && $data['contact_email_copy'];

            if ($params->get('n3t_store_messages'))
                $this->storeEmail($contact, $params, $name, $email, $subject, $body_text, $body, $data, null, $emailCopy);

            if ($emailCopy) {
                if ($custom_reply) {
                    $body	= $params->get('n3t_email_copy_template','');
                    $body_text = $params->get('n3t_email_copy_template_text','');
                    $head = $params->get('n3t_email_copy_template_head','');

                    $body = n3tContactEmail::processTemplate($data, $contact, $body, true);
                    $body_text = n3tContactEmail::processTemplate($data, $contact, $body_text);
                    $head = n3tContactEmail::processTemplate($data, $contact, $head);

                    $body_html = n3tContactEmail::html($body,$head,true);
                    $subject = $params->get('n3t_email_copy_subject', Text::sprintf('COM_CONTACT_COPYSUBJECT_OF', $data['contact_subject']));
                } else {
                    $body	= $params->get('n3t_email_template','');
                    $body_text = $params->get('n3t_email_template_text','');
                    $head = $params->get('n3t_email_copy_head','');

                    $body = n3tContactEmail::processTemplate($data, $contact, $body, true);
                    $body_text = n3tContactEmail::processTemplate($data, $contact, $body_text);
                    $head = n3tContactEmail::processTemplate($data, $contact, $head);

                    $body_html = n3tContactEmail::html($body, $head, true, Text::sprintf('COM_CONTACT_COPYTEXT_OF', $contact->name, $sitename));
                    $body_text = Text::sprintf('COM_CONTACT_COPYTEXT_OF', $contact->name, $sitename) . "\r\n\r\n" . $body_text;
                    $subject = Text::sprintf('COM_CONTACT_COPYSUBJECT_OF', $data['contact_subject']);
                }

                $replyEmail = $email;
                $replyName = $name;
                if ($params->get('n3t_email_copy_reply', '')) {
                    $replyEmail = $params->get('n3t_email_copy_reply', '');
                    $replyName = $params->get('n3t_email_copy_reply_name', '');
                }

                $mail = Factory::getMailer();
                $mail->isHTML(true);
                $mail->Encoding = 'base64';

                $mail->addRecipient($email, $name);
                $mail->addReplyTo($replyEmail, $replyName);
                $mail->setFrom($mailfrom, $fromname);
                $mail->setSubject($subject);
                $mail->setBody($body_html);
                $mail->AltBody = $body_text;
                $mail->Priority = $params->get('n3t_email_copy_priority', 3);

                $sent = $mail->Send();
            }
        }

        return $sent;
    }

    public function onSubmitContact(&$contact, &$data)
    {
        if ($contact) {
            $this->loadHelper('email');
            $params = JComponentHelper::getParams('com_contact');
            $params->merge($contact->params);

            $sent = $this->sendEmail($data, $contact, $params);

            if ((int)$params->get('n3t_acym_list'))
                $this->addAcymUser($data['contact_email'], $data['contact_name'], (int)$params->get('n3t_acym_list'));

            $custom_fields = n3tContactEmail::fields($data, $contact, false);
            if ($custom_fields) {
                $message = $data['contact_message'];
                $data['contact_message'] = $custom_fields;
                if ($message)
                    $data['contact_message'].= "\n" . Text::_('PLG_SYSTEM_N3TCONTACT_EMAIL_MESSAGE') . ': ' . "\n" . $message . "\n";
            }

            if ($sent instanceof Exception) {
                // todo - show error message
            }
        }
    }

    public function onAfterRoute() {
        $app = Factory::getApplication();
        if (Factory::getApplication()->isClient('site')) {
            if ($app->input->getCmd('option') == 'com_contact') {
                if ($app->input->getCmd('task') == 'contact.submit'
                    && $app->input->getCmd('format') == 'json') {
                    $base_path = JPATH_ROOT.'/components/com_contact';
                    if (Version::MAJOR_VERSION === 3) {
                        $controller = BaseController::getInstance('Contact', ['base_path' => __DIR__]);
                    } else {
                        require_once __DIR__ . '/src/Controller/ContactController.php';
                        $dispatcher = $app->bootComponent($app->input->getCmd('option'))->getDispatcher($app);
                        $controller = $dispatcher->getController('Contact', $app->getName(), ['base_path' => __DIR__, 'option' => 'com_contact', 'name' => 'Contact']);
                        $app->input->set('view', 'contact');
                    }

                    $controller->addViewPath($base_path . '/views');
                    $controller->addModelPath($base_path . '/models');
                } else  if ($app->input->get('view', '', 'cmd') == 'contact'
                    && $app->input->get('id', '0', 'uint')) {
                    $contact = $this->loadContact();
                    if ($contact && $contact->params->get('n3t_tmpl',''))
                        $app->input->set('tmpl',$contact->params->get('n3t_tmpl',''));
                }
                //$controller = JControllerLegacy::getInstance('Contact', array('base_path' => $base_path));
                //$document = Factory::getDocument();
                //$view = $controller->getView('contact', $document->getType(), '', array('base_path' => $base_path, 'layout' => $app->input->get('layout', 'default', 'string')));
                // a snad i template path
            }
        } elseif (Factory::getApplication()->isClient('administrator')) {
            if ($app->input->get('option', '', 'cmd') == 'com_contact') {
                if (Version::MAJOR_VERSION === 3) {
                    JHtmlSidebar::addEntry(Text::_('PLG_SYSTEM_N3TCONTACT_SIDEBAR_MESSAGES'), 'index.php?option=com_contact&view=messages', $app->input->get('view') == 'messages');
                } else {
                    Sidebar::addEntry(Text::_('PLG_SYSTEM_N3TCONTACT_SIDEBAR_MESSAGES'), 'index.php?option=com_contact&view=messages', $app->input->get('view') == 'messages');
                }

                $base_path = JPATH_ADMINISTRATOR.'/components/com_contact';
                $view = $app->input->get('view', '', 'cmd');
                $task = $app->input->get('task', '', 'cmd');
                if ($task) {
                    if (strpos($task, '.') !== false)
                        list ($view, $task) = explode('.', $task);
                    else
                        $view = $task;
                }

                if ($view == 'messages' || $view == 'message') {
                    require __DIR__ . '/controllers/message.php';
                    require __DIR__ . '/controllers/messages.php';

                    $controller = BaseController::getInstance('contact', array('base_path' => $base_path));
                    $controller->addViewPath(__DIR__ . '/views');
                    $controller->addModelPath(__DIR__ . '/models');
                }
            }
        }
    }

    function onAjaxN3tContactOptions() {
        if (Factory::getApplication()->isClient('site')) return false;
        return n3tContactField::getOptionsRow(JRequest::getCmd('type'));
    }

    function onAjaxN3tContactField() {
        if (Factory::getApplication()->isClient('site')) return false;
        return n3tContactField::getRow();
    }

    public function onContentPrepare($context, &$row, $params, $page = 0)  {
        if ( StringHelper::strpos( $row->text, '{n3tcontact' ) === false ) {
            return true;
        }

        Factory::getLanguage()->load('com_contact');

        require_once(__DIR__.'/views/n3tcontact/view.html.php');
        require_once JPATH_ROOT.'/components/com_contact/helpers/route.php';

        //JModelLegacy::addIncludePath(JPATH_ROOT.'/components/com_contact/models');
        BaseDatabaseModel::addIncludePath(__DIR__.'/models');
        Form::addFormPath(JPATH_ROOT.'/components/com_contact/models/forms');
        Table::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_contact/tables');

        $regex = '~{n3tcontact\s+([^\s\}]+)(\s([^}]*))?}~is';
        $row->text = preg_replace_callback($regex, array($this, 'loadContactForm'), $row->text);
    }

    protected function loadContactForm($matches) {
        $app = Factory::getApplication();
        if ($app->isClient('site')) {
            $alias = $matches[1];
            if (!$alias)
                return '';

            $db = Factory::getDbo();
            // Filter by start and end dates.
            $nullDate = $db->quote($db->getNullDate());
            $nowDate = $db->quote(Factory::getDate()->toSql());

            $query = $db->getQuery(true)
                ->select('id')
                ->from('#__contact_details')
                ->where('published = 1')
                ->where('(publish_up = ' . $nullDate . ' OR publish_up <= ' . $nowDate . ')')
                ->where('(publish_down = ' . $nullDate . ' OR publish_down >= ' . $nowDate . ')');

            if (preg_match('~^[0-9]+$~', $alias)) {
                $query->where('id = ' . (int)$alias);
            } else {
                $query->where('alias like ' . $db->quote($alias));
            }
            $db->setQuery($query);
            $id = $db->loadResult();

            if (!$id)
                return '';

            $this->contact_id = $id;

            $model = JModelLegacy::getInstance('n3tContact', 'ContactModel', array('ignore_request' => true));
            $model->setState('params', $app->getParams('com_contact'));
            $model->setState('contact.id', $id);

            $view = new ContactViewN3tContact(array('base_path' => __DIR__));
            $view->addTemplatePath(JPATH_THEMES.'/'.$app->getTemplate().'/html/com_contact/n3tcontact/');
            $view->setModel( $model, true );


            $layout = null;
            if (count($matches) > 3)
                $layout = $matches[3];
            if (!$layout)
                $layout = 'default';
            $view->setLayout($layout);

            return $view->display();
        }
    }

    private function initAcy()
    {
        if (function_exists('acym_get'))
            return true;
        $ds = DIRECTORY_SEPARATOR;
        $helperFile = rtrim(JPATH_ADMINISTRATOR, $ds) . $ds . 'components' . $ds . 'com_acym' . $ds . 'helpers' . $ds . 'helper.php';
        if (!file_exists($helperFile) || !include_once $helperFile)
            return false;

        return true;
    }


    private function addAcymUser($email, $name, $listId)
    {
        if (!$this->initAcy())
            return;

        $userClass = new \AcyMailing\Classes\UserClass();

        $user = new \stdClass();
        $user->email = $email;
        $user->name = $name;
        $user->confirmed = 1;
        $user->active = 1;

        $existUser = acym_loadObject('SELECT * FROM #__acym_user WHERE email = '.acym_escapeDB($user->email));
        if (!empty($existUser->id))
            $user->id = $existUser->id;
        $user->id = $userClass->save($user);
        $userClass->subscribe($user->id, $listId);
    }

    public function onPreprocessMenuItems(string $context, ?array &$items = null, ?Registry $params = null, bool $enabled = true) {
        if($context != 'com_menus.administrator.module')
            return;

        if (!$items)
            return;

        foreach ($items as $item) {
            if (property_exists($item, 'element') && $item->element == 'com_contact' && $item->level == 1) {
                $newItem = [
                    'title' => Text::_('PLG_SYSTEM_N3TCONTACT_SIDEBAR_MESSAGES'),
                    'type' => 'component',
                    'element' => 'com_contact',
                    'level' => 2,
                    'link' => 'index.php?option=com_contact&view=messages',
                    'class' => 'class:contact',
                    'browserNav' => '',
                    'submenu' => [],
                    'ajaxbadge' => null,
                    'dashboard' => null,
                ];
                if (Version::MAJOR_VERSION == 3) {
                    $newItem['params'] = clone $item->params;
                    $newItem = (object)$newItem;
                    $item->submenu[] = $newItem;
                } else {
                    /** @var AdministratorMenuItem $item */
                    $newItem['params'] = clone $item->getParams();
                    $newItem = new AdministratorMenuItem($newItem);
                    $item->addChild($newItem);
                }
            }
        }
    }
}


class plgButtonN3tContact extends CMSPlugin
{

    public function onDisplay($name)
    {
        $js = "
		function n3tContactSelectContact_" . $name . "(id, name, object)
		{
			var tag = '{n3tcontact ' + id + '}';
			jInsertEditorText(tag, '" . $name . "');
			SqueezeBox.close();
		}";

        $doc = Factory::getDocument();
        $doc->addScriptDeclaration($js);

        if (Version::MAJOR_VERSION == 3)
            HTMLHelper::_('behavior.modal');

        $link = 'index.php?option=com_contact&amp;view=contacts&amp;layout=modal&amp;tmpl=component&amp;function=n3tContactSelectContact_' . $name;

        $button = new JObject;
        $button->modal = true;
        $button->class = 'btn';
        $button->link = $link;
        $button->text = Text::_('PLG_SYSTEM_N3TCONTACT_EDITOR_BUTTON');
        $button->name = 'address';
        $button->options = "{handler: 'iframe', size: {x: 800, y: 500}}";
        return $button;
    }
}
