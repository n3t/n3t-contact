<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\CMS\MVC\Controller\FormController;

class ContactControllerContact extends FormController {

  public function getModel($name = 'Contact', $prefix = '', $config = ['ignore_request' => true]) {
    return parent::getModel($name, $prefix, array('ignore_request' => false));
  }

  public function getView($name = 'Contact', $type = '', $prefix = '', $config = [])
  {
    return parent::getView($name, $type, $prefix, $config);
  }

  public function submit() {
    try {
      if (!JSession::checkToken()) {
        echo new JResponseJson(null, JText::_('JINVALID_TOKEN'), true);
        return false;
      }

      $app    = JFactory::getApplication();
      $model  = $this->getModel('contact');
      $params = JComponentHelper::getParams('com_contact');
      $id     = $this->input->getInt('id');

      $data    = $this->input->post->get('jform', array(), 'array');
      $contact = $model->getItem($id);
      $app->setUserState('com_contact.contact.id', $id);

      $params->merge($contact->params);

      if ($params->get('validate_session', 0))
      {
        if (JFactory::getSession()->getState() != 'active')
        {
          $app->setUserState('com_contact.contact.data', $data);
          echo new JResponseJson(null, JText::_('COM_CONTACT_SESSION_INVALID'), true);
          return false;
        }
      }

      JPluginHelper::importPlugin('contact');
      $dispatcher = JEventDispatcher::getInstance();
      $form = $model->getForm();
      if (!$form)
      {
        echo new JResponseJson(null, $model->getError(), true);
        return false;
      }

      $validate = $model->validate($form, $data);

      if ($validate === false)
      {
        $errors  = $model->getErrors();
        for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
          if ($errors[$i] instanceof Exception)
            $app->enqueueMessage($errors[$i]->getMessage(), 'warning');
          else
            $app->enqueueMessage($errors[$i], 'warning');
        }
        $app->setUserState('com_contact.contact.data', $data);
        echo new JResponseJson(null, null, true);
        return false;
      }

      $results = $dispatcher->trigger('onValidateContact', array(&$contact, &$data));
      foreach ($results as $result)
      {
        if ($result instanceof Exception) {
          echo new JResponseJson($result);
          return false;
        }
      }

      $dispatcher->trigger('onSubmitContact', array(&$contact, &$data));
      $sent = false;

      if (!$params->get('custom_reply'))
        $sent = $this->_sendEmail($data, $contact, $params->get('show_email_copy'));

      if ($sent instanceof Exception) {
        echo new JResponseJson($sent);
        return false;
      }

      $app->setUserState('com_contact.contact.data', null);

      $response = array();
      if ($contact->params->get('n3t_use_thankyou', 0))
        $response['thankyou'] = n3tContactLayout::renderLayout($contact->params->get('n3t_thankyou_layout', 'n3tcontact.thankyou.default'),
          array('text' => $contact->params->get('n3t_thankyou'), 'contact' => $contact, 'data' => $data));

      echo new JResponseJson($response, JText::_($contact->params->get('n3t_success', 'COM_CONTACT_EMAIL_THANKS')));
      return true;
    }
    catch(Exception $e)
    {
      echo new JResponseJson($e);
      return false;
    }
  }

  private function _sendEmail($data, $contact, $copy_email_activated)
  {
      $app = JFactory::getApplication();

      if ($contact->email_to == '' && $contact->user_id != 0)
      {
        $contact_user      = JUser::getInstance($contact->user_id);
        $contact->email_to = $contact_user->get('email');
      }

      $mailfrom = $app->get('mailfrom');
      $fromname = $app->get('fromname');
      $sitename = $app->get('sitename');

      $name    = $data['contact_name'];
      $email   = JStringPunycode::emailToPunycode($data['contact_email']);
      $subject = $data['contact_subject'];
      $body    = $data['contact_message'];

      // Prepare email body
      $prefix = JText::sprintf('COM_CONTACT_ENQUIRY_TEXT', JUri::base());
      $body  = $prefix . "\n" . $name . ' <' . $email . '>' . "\r\n\r\n" . stripslashes($body);

      $mail = JFactory::getMailer();
      $mail->addRecipient($contact->email_to);
      if (!empty($email))
        $mail->addReplyTo($email, $name);
      $mail->setFrom($mailfrom, $fromname);
      $mail->setSubject($sitename . ': ' . $subject);
      $mail->setBody($body);
      $sent = $mail->Send();

      // If we are supposed to copy the sender, do so.

      // Check whether email copy function activated
      if ($copy_email_activated == true && !empty($data['contact_email_copy']) && !empty($email))
      {
        $copytext    = JText::sprintf('COM_CONTACT_COPYTEXT_OF', $contact->name, $sitename);
        $copytext    .= "\r\n\r\n" . $body;
        $copysubject = JText::sprintf('COM_CONTACT_COPYSUBJECT_OF', $subject);

        $mail = JFactory::getMailer();
        $mail->addRecipient($email, $name);
        $mail->addReplyTo($email, $name);
        $mail->setFrom($mailfrom, $fromname);
        $mail->setSubject($copysubject);
        $mail->setBody($copytext);
        $sent = $mail->Send();
      }

      return $sent;
  }
}
