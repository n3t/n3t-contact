<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class ContactModelMessage extends JModelAdmin
{
  public function getForm($data = array(), $loadData = true)
  {
    \JForm::addFormPath(__DIR__ . '/forms');
    $form = $this->loadForm('com_contact.message', 'message', array('control' => 'jform', 'load_data' => $loadData));

    return $form;
  }

	public function getTable($type = 'message', $prefix = 'n3tContactTable', $config = array())
	{
    JTable::addIncludePath(__DIR__ . '/../tables');
		$table = JTable::getInstance($type, $prefix, $config);
    return $table;
	}

}
