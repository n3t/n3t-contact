<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

use Joomla\CMS\Version;
use Joomla\Component\Contact\Site\Model\ContactModel;
use Joomla\CMS\Form\Form;

if (Version::MAJOR_VERSION === 3) {
  require_once JPATH_ROOT . '/components/com_contact/models/contact.php';

  class ContactModelN3tContact extends ContactModelContact
  {

    protected function loadForm($name, $source = null, $options = array(), $clear = false, $xpath = false)
    {
      // never cache form in JModelForm or in JForm
      Form::addFormPath(JPATH_ROOT . '/components/com_contact/models/forms');

      try {
        $form = new Form($name, $options);
        if ($form->loadFile($source, true, $xpath) == false)
          throw new \RuntimeException(sprintf('%s() could not load file', __METHOD__));
        $data = array();
        $this->preprocessForm($form, $data);
        $form->bind($data);
      } catch (\Exception $e) {
        $this->setError($e->getMessage());

        return false;
      }

      return $form;
    }

  }
} else {
  class ContactModelN3tContact extends ContactModel
  {
    protected function loadForm($name, $source = null, $options = array(), $clear = false, $xpath = false)
    {
      // never cache form in JModelForm or in JForm
      Form::addFormPath(JPATH_ROOT . '/components/com_contact/forms');

      try {
        $form = new Form($name, $options);
        if ($form->loadFile($source, true, $xpath) == false)
          throw new \RuntimeException(sprintf('%s() could not load file', __METHOD__));
        $data = array();
        $this->preprocessForm($form, $data);
        $form->bind($data);
      } catch (\Exception $e) {
        $this->setError($e->getMessage());

        return false;
      }

      return $form;
    }

  }
}
