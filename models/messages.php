<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class ContactModelMessages extends JModelList
{

	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
        'contact_id', 'a.contact_id',
        'contact_name', 'cd.name',
        'catid', 'cd.catid',
        'category_title', 'c.title',
        'name', 'a.name',
        'email', 'a.email',
				'subject', 'a.subject',
				'created', 'a.created',
				'email_copy', 'a.email_copy',
        'published', 'a.published',
			);
		}

		parent::__construct($config);
	}

	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);

		$categoryId = $this->getUserStateFromRequest($this->context . '.filter.category_id', 'filter_category_id');
		$this->setState('filter.category_id', $categoryId);

		$contactId = $this->getUserStateFromRequest($this->context . '.filter.contact_id', 'filter_contact_id');
		$this->setState('filter.contact_id', $contactId);

		parent::populateState('a.id', 'desc');
	}

	protected function getStoreId($id = '')
	{
		$id .= ':' . $this->getState('filter.search');
    $id .= ':' . $this->getState('filter.published');
    $id .= ':' . $this->getState('filter.category_id');
		$id .= ':' . $this->getState('filter.contact_id');

		return parent::getStoreId($id);
	}

	protected function getListQuery()
	{
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		$query->select(
			$this->getState(
				'list.select',
				'a.id, a.contact_id, a.name, a.email, a.subject, a.email_copy, a.created, a.published'					
			)
		);
		$query->from('#__n3tcontact_messages AS a');

		$query->select('cd.name AS contact_name, cd.catid as catid')
			->join('LEFT', '#__contact_details AS cd ON cd.id=a.contact_id');

		$query->select('c.title AS category_title')
			->join('LEFT', '#__categories AS c ON c.id = cd.catid');

		if (!$user->authorise('core.admin'))
		{
			$groups = implode(',', $user->getAuthorisedViewLevels());
			$query->where('cd.access IN (' . $groups . ')');
		}

		$contactId = $this->getState('filter.contact_id');
		if (is_numeric($contactId))
		{
			$query->where('a.contact_id = ' . (int) $contactId);
		}
		elseif (is_array($contactId))
		{
			JArrayHelper::toInteger($contactId);
			$contactId = implode(',', $contactId);
			$query->where('a.contact_id IN (' . $contactId . ')');
		}

		$categoryId = $this->getState('filter.category_id');
		if (is_numeric($categoryId))
		{
			$query->where('cd.catid = ' . (int) $categoryId);
		}
		elseif (is_array($categoryId))
		{
			JArrayHelper::toInteger($categoryId);
			$categoryId = implode(',', $categoryId);
			$query->where('cd.catid IN (' . $categoryId . ')');
		}

		$published = $this->getState('filter.published');
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('a.published = 1');
		}

		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			elseif (stripos($search, 'email:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 6), true) . '%');
				$query->where('(a.email LIKE ' . $search . ')');
			}
			else
			{
				$search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
				$query->where('(a.name LIKE ' . $search . ' OR a.email LIKE ' . $search . ' OR a.subject LIKE ' . $search . ')');
			}
		}

		$orderCol = $this->state->get('list.ordering', 'a.id');
		$orderDirn = $this->state->get('list.direction', 'desc');
		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;
	}
}
