/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

n3tContact = window.n3tContact || {};

n3tContact.renderMessages = function(messages, form) {
    var $ = jQuery.noConflict(),
      $container = $(form).find('.message-container'),
      $div, $h4, $divList, $p;
    $container.empty();

    $.each(messages, function(type, item) {
        $div = $('<div/>', {
            'class' : 'alert alert-' + type
        });
        $container.append($div)

        $h4 = $('<h4/>', {
            'class' : 'alert-heading',
            'text' : Joomla.JText._(type)
        });
        $div.append($h4);

        $divList = $('<div/>');
        $.each(item, function(index, item) {
            $p = $('<p/>', {
                html : item
            });
            $divList.append($p);
        });
        $div.append($divList);
    });

    n3tContact.scrollTo($container, parseInt(form.data('scroll-offset')));
};

n3tContact.renderMessage = function(type, message, form) {
  var messages = {};
  messages[type] = [message];
  n3tContact.renderMessages(messages, form);
};

n3tContact.scrollTo = function(el, offset) {
  el.get(0).scrollIntoView({
    behavior: 'smooth'
  });
}

n3tContact.ajaxForm = function(form) {
  var $ = jQuery.noConflict(),
    systemMessageContainer = $('#system-message-container'),
    container = form.find('.message-container'),
    buttons = form.find('input[type=submit],input[type=image],button[type=submit],button[type=image]');
  if (!container.length) {
    container = $('<div class="message-container" />');
    form.prepend(container);
  }

  /*var clicks = buttons.data('events').click.slice();
  buttons.unbind('click').click(function() {
    systemMessageContainer.attr('id', '');
    container.attr('id', 'system-message-container');
    container.empty();
  });
  $.each(clicks, function(i, v) {
    buttons.click(v);
  });
  buttons.click(function() {
    container.attr('id', '');
    systemMessageContainer.attr('id', 'system-message-container');
    if (!container.is(':empty'))
      n3tContact.scrollTo(container, parseInt(form.data('scroll-offset')));
  });*/

  form.submit(function (e) {
    form.addClass('n3tContactLoading');
    var url = form.attr('action');
    url += (url.indexOf('?') == -1) ? '?' : '&';
    url += 'format=json';
    var data = new FormData(this);
    jQuery.ajax({
      type: form.attr('method'),
      url: url,
      dataType: "json",
      async: true,
      data: data,
      contentType: false,
      processData: false,
    }).done(function(data){
      if (data.data && data.data.thankyou) {
        n3tContact.scrollTo($('<div class="n3tContactThankYou" />').html(data.data.thankyou).replaceAll(form), parseInt(form.data('scroll-offset')));
        form = null;
      } else if (data.messages)
        n3tContact.renderMessages(data.messages, form);
      else  if (data.message)
        n3tContact.renderMessage(data.success ? 'success' : 'warning', data.message, form);
      else if (!data.success)
        n3tContact.renderMessage('error', Joomla.JText._('PLG_SYSTEM_N3TCONTACT_JS_UNKNOWN_ERROR'), form);
      else
        n3tContact.renderMessage('success', Joomla.JText._('PLG_SYSTEM_N3TCONTACT_JS_SUCCESS'), form);
      if (form) {
        form.trigger('n3tcontact', true);
        if (data.success)
          form.trigger('reset');
      }
    }).fail(function(){
      n3tContact.renderMessage('error', Joomla.JText._('PLG_SYSTEM_N3TCONTACT_JS_AJAX_ERROR'), form);
      form.trigger('n3tcontact',false);
    }).always(function(){
      systemMessageContainer.attr('id', 'system-message-container');
      container.attr('id', '');

      if (form) {
        form.removeClass('n3tContactLoading');
        if (form.data('n3tseznamcaptcha'))
          form.data('n3tseznamcaptcha').reload();
      }
    });

    e.preventDefault();
  });
}

jQuery(function(){
  jQuery('form.n3tContactAjax').each(function(){
    n3tContact.ajaxForm(jQuery(this));
  });
});
