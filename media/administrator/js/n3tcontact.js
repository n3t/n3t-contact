/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

n3tContact = window.n3tContact || {};

n3tContact.recalculateNames = function() {
  jQuery("#n3tContactFields tbody").each(function(index, row) {  
    jQuery(row).find("input[name], select[name], textarea[name]").each(function(inputindex, input) {
      jQuery(input).attr("name", "jform[params][n3t_custom_fields]["+index+"]["+jQuery(input).attr("name").replace(/^.*\[(.*?)\]$/i,"$1")+"]");
		});
	});
}

n3tContact.ajaxContent = function(el) {
  // Chosen
  el.find("select").chosen({
    "disable_search_threshold": 10, 
    "allow_single_deselect": true, 
    "placeholder_text_multiple": Joomla.JText._('JGLOBAL_SELECT_SOME_OPTIONS'),
    "placeholder_text_single": Joomla.JText._('JGLOBAL_SELECT_AN_OPTION'),
    "no_results_text": Joomla.JText._('JGLOBAL_SELECT_NO_RESULTS_MATCH')
  });
  // Tooltips
  el.find(".hasTooltip").tooltip({"html": true,"container": "body"});
  // Radio button groups
	el.find('.radio.btn-group label').addClass('btn');
	el.find('.btn-group label:not(.active)').click(function(){
		var label = jQuery(this);
		var input = jQuery('#' + label.attr('for'));

		if (!input.prop('checked')) {
			label.closest('.btn-group').find('label').removeClass('active btn-success btn-danger btn-primary');
			if (input.val() == '') {
				label.addClass('active btn-primary');
			} else if (input.val() == 0) {
				label.addClass('active btn-danger');
			} else {
				label.addClass('active btn-success');
			}
			input.prop('checked', true);
		}
	});
	el.find('.btn-group input[checked=checked]').each(function(){
		if (jQuery(this).val() == '') {
			jQuery('label[for=' + jQuery(this).attr('id') + ']').addClass('active btn-primary');
		} else if (jQuery(this).val() == 0) {
			jQuery('label[for=' + jQuery(this).attr('id') + ']').addClass('active btn-danger');
		} else {
			jQuery('label[for=' + jQuery(this).attr('id') + ']').addClass('active btn-success');
		}
	});
  // Calendar  
	el.find('.n3t-contact-calendar').each(function(){
    $this = jQuery(this);
  	Calendar.setup({			
      inputField: this,			
      ifFormat: $this.data('calendar-format'),
      button: $this.next()[0],
      align: "Tl",
      singleClick: true,
      firstDay: $this.data('calendar-firstday')
  	});
  });
  //  Color picker
	el.find('.minicolors').each(function(){
		jQuery(this).minicolors({
			control: jQuery(this).attr('data-control') || 'hue',
			position: jQuery(this).attr('data-position') || 'right',
			theme: 'bootstrap'
		});
	});  
}


n3tContact.addField = function() {
  var row = jQuery('<tbody></tbody>');
  jQuery("#n3tContactFields").append(row);
  row.html('<tr><td colspan="1000"><div class="n3tContactLoading"></div></td></tr>');
  row.load("index.php?option=com_ajax&plugin=n3tcontactfield&format=raw", function(){    
    n3tContact.recalculateNames();    
    n3tContact.ajaxContent(row);
  });                  
}

n3tContact.removeField = function(btn) {
  var row = jQuery(btn).parents("tbody:first");  
  row.find('.hasTooltip').tooltip('destroy');
  row.remove();
  n3tContact.recalculateNames();
}

n3tContact.moveField = function(btn, direction) {
  var row = jQuery(btn).parents("tbody:first");
  var radios = row.find('input[type=radio]:checked');
  if (direction > 0) {
    var new_row = row.next("tbody");
    if (new_row) new_row.after(row);
  } else {
    var new_row = row.prev("tbody");
    if (new_row) new_row.before(row);
	}
  n3tContact.recalculateNames();
  radios.prop('checked', true);
}

n3tContact.toggleOptions = function(btn) {
  var tr = jQuery(btn).parents("tr:first").nextAll("tr.n3tCustomFieldOptions");
  if (tr.css("display") == "none")
    tr.css("display","table-row");
  else
    tr.css("display","none");
}

n3tContact.loadOptions = function(select) {
	var td = jQuery(select).parents("tr:first").nextAll("tr.n3tCustomFieldOptions").children("td:last");    
  td.html('<div class="n3tContactLoading"></div>');      
  td.load("index.php?option=com_ajax&plugin=n3tcontactoptions&format=raw&type="+jQuery(select).val(), function(){
    n3tContact.recalculateNames();
    n3tContact.ajaxContent(td);            
  });
}

jQuery(function(){
  n3tContact.recalculateNames();
});
