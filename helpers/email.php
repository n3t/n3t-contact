<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

class n3tContactEmail {
    
  public static function html($body, $head, $copy = false, $copy_text = false) {  
    return n3tContactLayout::renderLayout('n3tcontact.email.html',       
      array('body' => $body, 'head' => $head, 'copy' => $copy, 'copy_text' => $copy_text));
  }

  public static function fields($data, $contact, $html, $includeBasicFields = false) {
    $fields = $contact->params->get('n3t_custom_fields', []);
    if ($fields)
      foreach ($fields as $index => &$field)
        $field['index'] = $index;

    return n3tContactLayout::renderLayout('n3tcontact.email.'.($html ? 'htmlfields' : 'textfields'),       
      array('data' => $data, 'contact' => $contact, 'fields' => $fields, 'includeBasicFields' => $includeBasicFields));
  }
    
  public static function fieldValue($field, $value, $html = false) {
    if (static::isFile($field['type'])) {
      $value = array();
      $input = JFactory::getApplication()->input;
      $files = $input->files->get('jform');
      if (isset($files['n3t_custom_fields'])
      && is_array($files['n3t_custom_fields'])) {
        $files = $files['n3t_custom_fields'];
        if (isset($files['f'.$field['index']])) {
          $field_files = $files['f'.$field['index']];
          // single vs. multiple upload
          if (!isset($field_files[0]))
            $field_files = array($field_files);
          foreach($field_files as $file) {
            if ($file['error'])
              continue;
            $value[] = $file['name'];
          }
        }
      }
    }

    return n3tContactLayout::renderLayout('n3tcontact.email.'.($html ? 'htmlfield' : 'textfield'),       
      array('field' => $field, 'value' => $value));
  }
  
  public static function processTemplate($data, $contact, $template, $html = false) {
    $search = array(
      '[name]',
      '[email]',
      '[subject]',
      '[message]',
      '[sitename]',
      '[siteurl]',
      '[fields]',
      '[fieldsall]',
    );
    $replace = array(
      $data['contact_name'],
      $data['contact_email'],
      $data['contact_subject'],
      $html ? implode('<br />', preg_split('/\r?\n/', $data['contact_message'])) : $data['contact_message'],
      JFactory::getApplication()->get('sitename'),
      JUri::base(),
      static::fields($data, $contact, $html),
      static::fields($data, $contact, $html, true),
    );
    $custom_fields = $contact->params->get('n3t_custom_fields',null);
    if ($custom_fields) {
      foreach ($custom_fields as $index => &$field) {
        $field['index'] = $index;
        $value = static::fieldValue($field, @$data['n3t_custom_fields']['f'.$index], $html);
        $search[] = '[field '.$field['label'].']';
        $replace[] = $value; 
        $search[] = '[fieldindex '.($index+1).']';
        $replace[] = $value;
      }
    }
    
    return str_replace($search, $replace, $template);      
  }
  
  public static function processAttachments($contact, $mail) {    
    $custom_fields = $contact->params->get('n3t_custom_fields',null);
    if ($custom_fields) {
      $input = JFactory::getApplication()->input;
      $files = $input->files->get('jform');
      if (isset($files['n3t_custom_fields'])
      && is_array($files['n3t_custom_fields'])) {
        $files = $files['n3t_custom_fields'];
        foreach ($custom_fields as $index => $field) {
          if (!static::isFile($field['type'])) continue;
          if (isset($files['f'.$index])) {
            $field_files = $files['f'.$index];
            // single vs. multiple upload
            if (!isset($field_files[0])) $field_files = array($field_files); 
            foreach($field_files as $file) {
              if ($file['error']) continue;
              $mail->addAttachment($file['tmp_name'],$file['name'],'base64',$file['type']);
            }
          }
        }
      }
    }        
  }

  public static function isHidden($type) {
    return in_array($type,array('note', 'spacer'));
  }

  public static function isFile($type) {
    return in_array($type, array('file'));
  }
} 