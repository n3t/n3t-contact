<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

use Joomla\CMS\Form\Form;

defined('_JEXEC') or die;

class n3tContactField {

  public static function createUniqueId(&$field) {
    $field->id = uniqid();
  }

  public static function getOptionsRow($type, $data = array(), $index = 0) {
    Form::addFormPath(__DIR__ . '/../forms/options');
    $form = new Form('n3tcontact.options.'.$type, array('control' => 'jform[params][n3t_custom_fields]['.$index.']'));
    $form->loadFile($type);
    $form->bind($data);
    return n3tContactLayout::renderLayout('n3tcontact.edit.options',
      array('form' => $form, 'data' => $data, 'index' => $index));
  }

  public static function getRow($data = array(), $index = 0) {
    Form::addFormPath(__DIR__ . '/../forms');
    $form = new Form('n3tcontact.field', array('control' => 'jform[params][n3t_custom_fields]['.$index.']'));
    $form->loadFile('field');
    $form->bind($data);
    return n3tContactLayout::renderLayout('n3tcontact.edit.row',
      array('form' => $form, 'data' => $data, 'index' => $index));
  }

  public static function getHeader() {
    Form::addFormPath(__DIR__ . '/../forms');
    $form = new Form('n3tcontact.field', array('control' => 'jform[params][n3t_custom_fields][]'));
    $form->loadFile('field');
    return n3tContactLayout::renderLayout('n3tcontact.edit.header',
      array('form' => $form));
  }

  public static function getButtons($data = array()) {
    return n3tContactLayout::renderLayout('n3tcontact.edit.buttons');
  }

  public static function getToolbar($data = array()) {
    return n3tContactLayout::renderLayout('n3tcontact.edit.toolbar');
  }

  public static function getTable($rows = array()) {
    return n3tContactLayout::renderLayout('n3tcontact.edit.table',
      array('rows' => $rows));
  }

}
