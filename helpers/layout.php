<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

use Joomla\CMS\Factory;
use Joomla\CMS\Layout\FileLayout;

defined('_JEXEC') or die;

class n3tContactLayout extends FileLayout {

    public static function renderLayout($layoutFile, $displayData = null) {
        $layout = new n3tContactLayout($layoutFile);
        $layout->clearIncludePaths();
        $layout->addIncludePaths(JPATH_PLUGINS . '/system/n3tcontact/layouts');
        $layout->addIncludePaths(JPATH_THEMES . '/' . Factory::getApplication()->getTemplate() . '/html/layouts');

        return $layout->render($displayData);
    }

}
