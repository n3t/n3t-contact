<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

use Joomla\CMS\Plugin\PluginHelper;

defined('_JEXEC') or die;

class n3tContactPlugin extends PluginHelper {

  public static function addPluginType($type, $plugin, $new_type) {
    $plugin = static::getPlugin($type, $plugin);
    $new_plugin = static::getPlugin($new_type, $plugin);
    if ($plugin && !$new_plugin) {
      $new_plugin = clone $plugin;
      $new_plugin->type = $new_type;
      static::$plugins[] = $new_plugin;
    }
  }

}
