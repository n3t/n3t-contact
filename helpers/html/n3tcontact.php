<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined('_JEXEC') or die;

abstract class JHtmlN3tContact
{

	public static function jgridContactOptions($catid = null)
	{
    return array();
	}
                              
  public static function jgridPublished($state)
  {
    $html = array();
    
		$states = array(
      0 => 'publish',
      1 => 'publish',			
			2 => 'archive',
			-2 => 'trash'
    );    
    
		$html[] = '<span class="btn btn-micro disabled">';
		$html[] = '<i class="icon-' . $states[$state] . '">';
		$html[] = '</i>';
		$html[] = '</span>';
    
    return implode($html);    
  }
                 
}
