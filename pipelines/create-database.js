var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root'
});

connection.connect();

connection.query('CREATE DATABASE IF NOT EXISTS joomla COLLATE utf8_general_ci;', function(err, rows, fields) {
  if (err) throw err;
});

connection.end();