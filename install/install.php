<?php
/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 **/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Version;
use Joomla\Registry\Registry;

class plgSystemN3tContactInstallerScript
{
	private const MIN_PHP_VERSION = '7.4.0';
	private const MIN_JOOMLA_VERSION = '3.10.0';

  var $oldVersion = null;
  var $newVersion = null;
  var $currentExtensionId = null;

	/**
	 * Enqueues error message
	 * @param $text
	 * @throws Exception
	 *
	 * @since 4.0.0
	 */
	private function enqueueError($text)
	{
		$app = Factory::getApplication();
		$app->enqueueMessage($text, 'error');
	}

	/**
	 * Checks minimal PHP version
	 * @return bool
	 *
	 * @since 4.0.0
	 */
	private function checkPHPVersion()
	{
		if (version_compare(phpversion(),self::MIN_PHP_VERSION, '<')) {
			$this->enqueueError(Text::sprintf('PLG_SYSTEM_N3TCONTACT_INSTALL_PHP_VERSION', phpversion(), self::MIN_PHP_VERSION));
			return false;
		}

		return true;
	}

	/**
	 * Checks minimal Joomla version
	 * @return bool
	 *
	 * @since 4.0.0
	 */
	private function checkJoomlaVersion()
	{
		$version = new Version;
		if (version_compare($version->getShortVersion(),self::MIN_JOOMLA_VERSION, '<')) {
			$this->enqueueError(Text::sprintf('PLG_SYSTEM_N3TCONTACT_INSTALL_PHP_VERSION', $version->getShortVersion(), self::MIN_JOOMLA_VERSION));
			return false;
		}

		return true;
	}

	/**
	 * Runs just before any installation action is performed on the component.
	 * Verifications and pre-requisites should run in this function.
	 *
	 * @param  string    $type   - Type of PreFlight action. Possible values are:
	 *                           - * install
	 *                           - * update
	 *                           - * discover_install
	 * @param  \stdClass $parent - Parent object calling object.
	 *
	 * @return void
	 * @since 4.0.0
	 */
  public function preflight($type, $parent)
  {
		if (!$this->checkPHPVersion())
			return false;

	  if (!$this->checkJoomlaVersion())
		  return false;

    if ($type == 'update') {
      $db = Factory::getDbo();
      $query = $db->getQuery(true)
        ->select($db->quoteName(['manifest_cache', 'extension_id']))
        ->from($db->quoteName('#__extensions'))
        ->where($db->quoteName('name') . ' = ' . $db->quote($parent->getName()));
      $db->setQuery($query);
      $extension = $db->loadObject();

      $this->currentExtensionId = $extension->extension_id;
      $manifest = new Registry($extension->manifest_cache);
      $this->oldVersion = $manifest->get('version');
    }

		return true;
  }

	/**
	 * Store default plugin parameters
	 * @param  \stdClass $parent - Parent object calling object.
	 *
	 * @since 4.0.0
	 */
	private function storeDefaultParams($parent)
	{
		$params = new Registry();

		$blocks = [];
		$blocks[] = (object)['type' => 'description'];
		$blocks[] = (object)['type' => 'functional'];
		$blocks[] = (object)['type' => 'preferences'];
		$blocks[] = (object)['type' => 'analytics'];
		$blocks[] = (object)['type' => 'marketing'];
		$blocks[] = (object)['type' => 'unknown'];
		$blocks[] = (object)['type' => 'hidden'];
		$blocks[] = (object)['type' => 'system'];
		$blocks[] = (object)['type' => 'privacy'];
		$blocks[] = (object)['type' => 'consent'];
		$params->set('blocks', $blocks);

		$db = Factory::getDbo();
		$query = $db->getQuery(true);
		$query->update('#__extensions')
			->set('params=' . $db->quote((string)$params))
			->where($db->quoteName('name') . ' = ' . $db->quote($parent->getName()));
		$db->setQuery($query)->execute();
	}

	/**
	 * Runs right after any installation action is performed on the component.
	 *
	 * @param  string    $type   - Type of PostFlight action. Possible values are:
	 *                           - * install
	 *                           - * update
	 *                           - * discover_install
	 * @param  \stdClass $parent - Parent object calling object.
	 *
	 * @return void
	 * @since 4.0.0
	 */
	public function postflight($type, $parent)
	{
		if ($type === 'install')
			$this->storeDefaultParams($parent);
	}

	/**
	 * This method is called after a component is updated.
	 *
	 * @param  \stdClass $parent - Parent object calling object.
	 *
	 * @return void
	 * @since 4.0.0
	 */
	public function update($parent)
	{
    $this->newVersion = $parent->getManifest()->version;
    Factory::getApplication()->enqueueMessage(Text::sprintf('PLG_SYSTEM_N3TCONTACT_INSTALL_UPDATE_VERSION', $this->oldVersion, $this->newVersion));

  	$this->checkUpdateServers($parent);
	}

	/**
	 * checks update servers
	 *
	 * @param  \stdClass $parent - Parent object calling object.
	 *
	 * @return void
	 * @since 4.0.0
	 */
  protected function checkUpdateServers($parent)
	{
    $eid = $this->currentExtensionId;
		$manifest = $parent->getManifest();

		$updateservers	= $manifest->updateservers;
		if ($updateservers)
      $updateservers = $updateservers->children();
		else
			$updateservers = array();

    $locations = array();
		foreach ($updateservers as $updateserver)
      $locations[] = trim($updateserver);

    $db = Factory::getDbo();

    $query = $db->getQuery(true)
      ->select($db->quoteName(array('a.update_site_id', 'a.location')))
      ->from($db->quoteName('#__update_sites', 'a'))
      ->join('INNER', $db->quoteName('#__update_sites_extensions', 'b') . ' ON (' . $db->quoteName('a.update_site_id') . ' = ' . $db->quoteName('b.update_site_id') . ')')
      ->group($db->quoteName(array('a.update_site_id', 'a.location')))
      ->where($db->quoteName('b.extension_id') . '=' . $eid);
    $current_sites = $db->setQuery($query)->loadObjectList();

    $delete_sites_extensions = array();
    $delete_sites = array();
    foreach($current_sites as $current_site) {
      if (!in_array($current_site->location, $locations)) {
        $delete_sites_extensions[] = $current_site->update_site_id;
        $query->clear()
          ->select('count('.$db->quoteName('extension_id').')')
          ->from($db->quoteName('#__update_sites_extensions'))
          ->where($db->quoteName('update_site_id') . '=' . $current_site->update_site_id)
          ->where($db->quoteName('extension_id') . '!=' . $eid);
        $current_site->extensions_count = $db->setQuery($query)->loadResult();

        if ($current_site->extensions_count == 0)
          $delete_sites[] = $current_site->update_site_id;
      }
    }

    if (count($delete_sites_extensions)) {
      $query->clear()
        ->delete($db->quoteName('#__update_sites_extensions'))
        ->where(array(
          $db->quoteName('extension_id') . '=' . $eid,
          $db->quoteName('update_site_id') . ' in (' . implode(',', $delete_sites_extensions) . ')'
        ));
      $db->setQuery($query)->execute();
    }

    if (count($delete_sites)) {
      $query->clear()
        ->delete($db->quoteName('#__update_sites'))
        ->where($db->quoteName('update_site_id') . ' in (' . implode(',', $delete_sites) . ')');
      $db->setQuery($query)->execute();
    }
	}

}
