/**
 * @package n3t Contact plugin
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2014 - 2018 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

ALTER TABLE `#__n3tcontact_messages` CHANGE `text` `text` MEDIUMTEXT;
ALTER TABLE `#__n3tcontact_messages` CHANGE `html` `html` MEDIUMTEXT;
ALTER TABLE `#__n3tcontact_messages` ADD `attachments` MEDIUMTEXT AFTER `html`;
ALTER TABLE `#__n3tcontact_messages` ADD `values` MEDIUMTEXT AFTER `html`;